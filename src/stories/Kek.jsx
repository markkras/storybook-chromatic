import React from 'react';
import PropTypes from 'prop-types';
import './button.css';

/**
 * Primary UI component for user interaction
 */
export const Kek = ({ primary, backgroundColor, size, isShown, label, ...props }) => {
// const [isShown, setShown] = React.useState();
  return (
    <div style={isShown ? {} : { display: 'none' }} onClick={() => isShown = false}>
        { label }
    </div>
  );
};

Kek.propTypes = {
  isShown: PropTypes.bool,
  /**
   * Is this the principal call to action on the page?
   */
  primary: PropTypes.bool,
  /**
   * What background color to use
   */
  backgroundColor: PropTypes.string,
  /**
   * How large should the button be?
   */
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  /**
   * Button contents
   */
  label: PropTypes.string.isRequired,
  /**
   * Optional click handler
   */
  onClick: PropTypes.func,
};

Kek.defaultProps = {
  backgroundColor: null,
  primary: false,
  size: 'medium',
  onClick: undefined,
};
